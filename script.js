
let playGame = true;
let ronde = 4;
  
alert("Selamat datang di permainan tebak angka" + "\n" + 
        "Aturan bermain: " + "\n" + "1. Terdiri dari 2 pemain dalam 4 ronde" + "\n" + 
        "2. Silahkan tebak angka antara 1-3" + "\n" +
        "3. Jika memasukan selain angka 1-3 akan kembali ke awal");

function tebakAngka() {
    let angka = Math.floor(Math.random()*4)
    while (playGame) {
    let player1 = Number(prompt('Player 1: tebak angka dari mulai 1 hingga 3'));
    if (!player1 || player1 < 0 || player1 > 3) {
        alert("Mohon masukan angka antara 1-3");
        askAgain();
    }
    let player2 = Number(prompt('Player 2: tebak angka dari mulai 1 hingga 3'));
    if (!player2 || player2 < 0 || player2 > 3) {
        alert("Mohon masukan angka antara 1-3");
        askAgain();
    }
    let validate = setGames(player1, player2, angka);
    if (validate == false) alert(`Tersisa ${ronde} ronde lagi!`); 
    if (ronde == 0) {
        alert('Game sudah selesai');
        askAgain();
    }
    }
}

tebakAngka()

alert('Terima kasih sudah bermain game tebak angka :)');

function setGames(player1, player2, angka) {
    if (player1 == player2) {
    ronde--;
    alert('Tebakan tidak boleh sama');
    return false;
    }
    if (player1 != angka && player2 != angka) {
    ronde--;
    alert('Tidak ada yang benar. Hasil SERI');
    return false;
    }
    if (player1 = angka && player2 != angka) {
    alert('Player 1 win!');
    alert(`Jawaban yang benar adalah ${angka}`);
    return askAgain();
    }
    if (player2 = angka && player1 != angka) {
    alert('Player 2 win!');
    alert(`Jawaban yang benar adalah ${angka}`);
    return askAgain();
    }
}

function askAgain() {
    let ask = confirm('Mau mencoba bermain lagi?');
    if (ask == true) {
    ronde = 4;
    tebakAngka();
    } else {
    playGame = false;
    }
}
